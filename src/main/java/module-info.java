/**
 * Module-info for the project
 */
module edu.ntnu.oflarsen.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;


    opens edu.ntnu.oflarsen.idatt2001 to javafx.fxml;
    exports edu.ntnu.oflarsen.idatt2001;
    exports edu.ntnu.oflarsen.idatt2001.oblig3.cardgame;
}