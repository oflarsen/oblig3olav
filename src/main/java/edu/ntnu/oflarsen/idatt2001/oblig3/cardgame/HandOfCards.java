package edu.ntnu.oflarsen.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The class HandOfCards.
 * The class is created because this makes it easier to have your hand being checked for different characteristics
 */
public class HandOfCards {
    private Collection<PlayingCard> myHand;

    public HandOfCards(Collection<PlayingCard> myHand){
        this.myHand = myHand;
    }

    public Collection<PlayingCard> getMyHand() {
        return myHand;
    }

    /**
     * Method which uses lambda and stream to sum all the faces of your hand
     * @return sum
     */
    public int sumOfHand(){
        int sum = myHand.stream().map(PlayingCard::getFace).mapToInt(s -> s).sum();
        return sum;
    }

    /**
     * Uses lambda and stream to filter through all the PlayingCards in your hand, and returns a List of all hearts
     * @return
     */
    public List<PlayingCard> getHearts(){

        List<PlayingCard> hearts = myHand
                .stream()
                .filter(h -> h.getSuit() == 'H')
                .collect(Collectors.toList());
        return hearts;
    }

    /**
     * Method that loops through your hand, returns a boolean based on if your hand contains queenOfSpades
     * @return
     */
    public boolean queenOfSpades(){
        return myHand.stream().anyMatch(q -> q.getSuit() == 'S' && q.getFace() == 12);
    }

    /**
     * Loops through your hand and checks for each suit, if the hand contains more than five of a suit
     * @return boolean, that is decided by if your hand contains five or more of the same suit
     */
    public boolean fiveFlush(){
        for(int y = 0; y < DeckOfCards.getSuits().length; y++){
            int finalY = y;
            if(myHand.stream()
                    .filter(s -> s.getSuit() == DeckOfCards.getSuits()[finalY]).collect(Collectors.toList()).size() >= 5){
                return true;
            }

        }
        return false;
    }

    /**
     * toString method that returns your hand to a String like: SuitFace, SuitFace....
     * @return
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        for(PlayingCard p : myHand){
            i++;
            if(i < 2){
                stringBuilder.append(p.toString());
            }else {
                stringBuilder.append(", " + p.toString());
            }
        }
        return stringBuilder.toString();
    }
}
