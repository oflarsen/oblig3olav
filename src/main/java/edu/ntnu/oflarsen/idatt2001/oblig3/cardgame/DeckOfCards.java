package edu.ntnu.oflarsen.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * DeckOfCards class
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> deckOfCards;

    public static Character[] getSuits() {
        return suit;
    }

    private static final Character[] suit = {'S', 'H', 'D', 'C'};

    /**
     * Creates an instance of DeckOfCards. Creates a new deck of cards, with 52 different cards
     */
    public DeckOfCards(){
        deckOfCards = new ArrayList<>();

        // Creates a new deck of cards when creating an instance of the class
        for (int y = 0; y<suit.length; y++) {
            for (int i = 1; i <= 13; i++) {
                deckOfCards.add(new PlayingCard(suit[y],i));
            }
        }
    }

    /**
     * Method to return a hand with n random cards from the deck. You can't get dealt more than 52, or less than 1
     * The method also checks if the random card picked, already is picked. If the card already is dealt, the loop continues, if
     * the card is not dealt, then it gets added to the ArrayList, cardsToDeal, which is returned to the user
     * @param n
     * @return
     */
    public Collection<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> cardsToDeal = new ArrayList<>();
        Random random = new Random();
        // Throws IllegalArgumentException if n > 52, or n < 1
        if(n > 52 || n < 1){
            throw new IllegalArgumentException("Can't deal a hand larger or smaller than the deck");
        }

        for(int i = 0; i < n; i++){
            PlayingCard playingCard = deckOfCards.get(random.nextInt(52));
            if(!cardsToDeal.contains(playingCard)){
                cardsToDeal.add(playingCard);
            }else{
                // If the card is already dealt, make sure the loop, loops one more time
                i--;
            }
        }
        return cardsToDeal;
    }

    public ArrayList<PlayingCard> getDeckOfCards() {
        return deckOfCards;
    }
}
