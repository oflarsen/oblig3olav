package edu.ntnu.oflarsen.idatt2001;

import edu.ntnu.oflarsen.idatt2001.oblig3.cardgame.DeckOfCards;
import edu.ntnu.oflarsen.idatt2001.oblig3.cardgame.HandOfCards;
import edu.ntnu.oflarsen.idatt2001.oblig3.cardgame.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;

/**
 * Main controller for JavaFx
 */
public class Controller {
    private HandOfCards myHand;
    @FXML
    private TextField flush;
    @FXML
    private TextField queenOfSpades;
    @FXML
    private TextField sumOfFaces;
    @FXML
    private TextField cardsOfHeart;
    @FXML
    private TextField hand;
    @FXML
    private Button btnCheckHand;

    /**
     * Method that deals a new hand to the user, and enables the possibility to check the dealt hand
     */
    @FXML
    private void dealHand(){
        DeckOfCards deckOfCards = new DeckOfCards();
        this.myHand = new HandOfCards(deckOfCards.dealHand(7));

        btnCheckHand.setDisable(false);
        hand.setText(myHand.toString());
    }

    /**
     * Method that checks if the dealt hand:
     * - Is flush
     * - Has queen of spades
     * and returns the sum of faces and all the cards with the suit heart
     * @throws IOException
     */
    @FXML
    private void checkHand() throws IOException{
        int i = 0;
        if(myHand.fiveFlush()){
            flush.setText("Yes");
        }else {
            flush.setText("No");
        }

        if(myHand.queenOfSpades()){
            queenOfSpades.setText("Yes");
        }else{
            queenOfSpades.setText("No");
        }
        sumOfFaces.setText(Integer.toString(myHand.sumOfHand()));

        StringBuilder stringBuilder = new StringBuilder();
        for(PlayingCard p : myHand.getHearts()){
            i++;
            if(i<2){
                stringBuilder.append(p.toString());
            }else {
                stringBuilder.append(", " + p.toString());
            }
        }
        if(stringBuilder.length() == 0){
            stringBuilder.append("No Hearts");
        }

        cardsOfHeart.setText(stringBuilder.toString());

    }

}
