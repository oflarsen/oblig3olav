package edu.ntnu.oflarsen.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {



    @Test
    void sumOfHand() {
        DeckOfCards deckOfCards = new DeckOfCards();
        HandOfCards handOfCards = new HandOfCards(deckOfCards.dealHand(5));

        int sum = 0;
        for (PlayingCard p: handOfCards.getMyHand()
        ) {
            sum += p.getFace();
        }

        assertEquals(handOfCards.sumOfHand(), sum);
    }

    @Test
    void getHearts() {
        DeckOfCards deckOfCards = new DeckOfCards();
        HandOfCards handOfCards = new HandOfCards(deckOfCards.dealHand(10));
        ArrayList<PlayingCard> hearts = new ArrayList<>();

        for (PlayingCard p:handOfCards.getMyHand()
             ) {
            if(p.getSuit() == 'H')hearts.add(p);
        }

        assertEquals(handOfCards.getHearts().size(), hearts.size());
    }

    @Test
    void queenOfSpades() {
        DeckOfCards deckOfCards = new DeckOfCards();
        HandOfCards handOfCards = new HandOfCards(deckOfCards.dealHand(10));
        boolean queenOfSpade = handOfCards.getMyHand().contains(new PlayingCard('S',12));

        assertEquals(handOfCards.queenOfSpades(), queenOfSpade);
    }

    @Test
    void fiveFlush() {
        DeckOfCards deckOfCards = new DeckOfCards();
        HandOfCards handOfCards = new HandOfCards(deckOfCards.dealHand(10));
        boolean test = false;
        int sum = 0;
        for(int i = 0; i < DeckOfCards.getSuits().length; i++){
            for (PlayingCard p: handOfCards.getMyHand()
                 ) {
                if(p.getSuit() == DeckOfCards.getSuits()[i]){
                    sum++;
                }
            }
            if(sum >= 5){
                test = true;
            }
            sum = 0;
        }

        assertEquals(handOfCards.fiveFlush(), test);
    }
}